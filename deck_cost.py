#!/usr/bin/env python3
import lor_deckcodes
import json
import glob
import os
import requests
import argparse
import sys
import getpass
from rich import box
from rich.table import Column
from rich.console import Console
from rich.table import Table

rarity_colors = {
    "Common": "white",
    "Rare": "blue",
    "Epic": "purple",
    "Champion": "red",
}

def login(email, password, token=False):
    if not os.path.isfile("token.txt"):
        cards = create_token(email, password)
    elif token == True:
        with open("token.txt", "r") as f:
            auth = f.read()
            cards = use_token(auth)
    return cards["cards"]

        
def use_token(auth):
    with requests.Session() as s:
        headers = {
            'authorization': f"Bearer {auth}"
        }
        response = s.get('https://lor.mobalytics.gg/api/v2/riot/cards', headers=headers)
        cards = json.loads(response.text)
    return cards
        
def create_token(email, password):
    url = "https://account.mobalytics.gg/api/graphql/v1/query"
    payload = '{"operationName":"SignIn","variables":{"email":"'+email+'","password":"'+password+'"},"query":"mutation SignIn($email: String!, $password: String!, $continueFrom: String) { signIn(email: $email, password: $password, continueFrom: $continueFrom)}"}'
    headers = {
        'Accept': "*/*",
        'Accept-Language': "en_us",
        'Referer': "https://account.mobalytics.gg/sign-in?lang=en_us&redirect_uri=https%3A%2F%2Flor.mobalytics.gg%2Fauth-result%3Fnext%3D%252F&theme=bacon",
        'content-type': "application/json",
        'x-moba-proxy-gql-ops-name': "signIn",
        'Origin': "https://account.mobalytics.gg",
    }
    with requests.Session() as s:
        response = s.post(url, data=payload, headers=headers)
        response = s.get('https://lor.mobalytics.gg/api/v2/riot/cards')
        cards = json.loads(response.text)
        payload = "{\"operationName\":\"Account\",\"variables\":{},\"query\":\"query Account {\\n  account {\\n    ...AccountFragment\\n    __typename\\n  }\\n  auth {\\n    token\\n    __typename\\n  }\\n}\\n\\nfragment AccountFragment on Account {\\n  uid\\n  email\\n  login\\n  loginHash\\n  level\\n  iconUrl\\n  connectedAuthProviders {\\n    type\\n    displayName\\n    __typename\\n  }\\n  settings {\\n    ...MetaDataSettingsFragment\\n    __typename\\n  }\\n  attributes {\\n    ...MetaDataAttributesFragment\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment MetaDataSettingsFragment on MetaDataSettingsKV {\\n  key\\n  value\\n  createdAt\\n  updatedAt\\n  __typename\\n}\\n\\nfragment MetaDataAttributesFragment on MetaDataAttributesKV {\\n  key\\n  value\\n  createdAt\\n  updatedAt\\n  __typename\\n}\\n\"}"
        headers = {
            'Accept': "*/*",
            'Accept-Language': "en_us",
            'Referer': "https://lor.mobalytics.gg/cards/",
            'content-type': "application/json",
            'x-moba-proxy-gql-ops-name': "fetchAccount",
            }
        response = s.post(url, data=payload, headers=headers)
        auth = json.loads(response.text)
        auth = auth["data"]["auth"]["token"]
        if auth:
            with open("token.txt", "w") as f:
                f.write(auth)
    return cards


def deck_code(code, library):
    current_cards = lor_deckcodes.LoRDeck.from_deckcode(library)
    sets = open_assets()
    deck = lor_deckcodes.LoRDeck.from_deckcode(code)
    rarity_dict = {}
    for c_card in current_cards.cards:
        for card in deck.cards:
            if card.card_code == c_card.card_code:
                card.count = card.count - c_card.count
                if card.count < 0:
                    card.count = 0
    table = Table(
        "Name",
        Column(header="Needed", justify="right"),
        title="Cards Needed",
        header_style="bold green",
        show_header=True
    )
    for lor in sets:
        for card in deck.cards:
            for i in range(len(lor)):
                if card.card_code == lor[i]["cardCode"]:
                    table.add_row(f'[{rarity_colors[lor[i]["rarity"].lower().capitalize()]}]{lor[i]["name"]}[/{rarity_colors[lor[i]["rarity"].lower().capitalize()]}]', 
                        str(card.count))
                    if lor[i]["rarity"] in rarity_dict:
                        rarity_dict[lor[i]["rarity"]] += card.count
                    else:
                        rarity_dict[lor[i]["rarity"]] = card.count
    console = Console()
    console.print(table)
    return rarity_dict


def open_assets():
    result = []
    for f in glob.glob("*.json"):
        with open(f, "r", encoding="utf8") as infile:
            result.append(json.load(infile))
    return result


def ask_cards(code, library):
    print()
    rarity_deck = deck_code(code, library)
    total = 0
    rich_rows = []
    r_totals = {
        "champion": 0,
        "epic": 0,
        "rare": 0,
        "common": 0,
    }
    rarity_deck = {key.lower() if type(key) == str else key: value for key, value in rarity_deck.items()}
    for rarity,number in rarity_deck.items():
        if number > 0:
            try:
                owned = int(input(f"How many {rarity.capitalize()} Wildcards do you own: "))
            except Exception:
                owned = 0
            rarity_deck[rarity] = number - owned
            if rarity_deck[rarity] < 0:
                rarity_deck[rarity] = 0
        if rarity == "champion":
            total += (rarity_deck[rarity] * 300) / 100
            r_totals[rarity] = (rarity_deck[rarity] * 300) / 100
        elif rarity == "epic":
            total += (rarity_deck[rarity] * 120) / 100
            r_totals[rarity] = (rarity_deck[rarity] * 120) / 100
        elif rarity == "rare":
            total += (rarity_deck[rarity] * 30) / 100
            r_totals[rarity] = (rarity_deck[rarity] * 30) / 100
        elif rarity == "common":
            total += (rarity_deck[rarity] * 10) / 100
            r_totals[rarity] = (rarity_deck[rarity] * 10) / 100
        rich_rows.append((f'[{rarity_colors[rarity.capitalize()]}]'+rarity.capitalize()+f'[/{rarity_colors[rarity.capitalize()]}]', 
            str(rarity_deck[rarity]), '${:,.2f}'.format(r_totals[rarity])))
    table = Table(
        "Rarity",
        Column(header="Amount", justify="right", footer="[b]Total"),
        Column(header="Cost", justify="right", footer="[b][red]${:,.2f}".format(total)),
        title="Rarity Needed",
        header_style="bold green",
        show_header=True,
        show_footer=True
    )
    for row in rich_rows:
        table.add_row(row[0], row[1], row[2])
    print()
    console = Console()
    console.print(table)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--email", help="Your mobalytics email", action="store_true")
    parser.add_argument("-d", "--deck", help="Deckcode of deck you want to use", type=str)
    args = parser.parse_args()
    if not os.path.isfile("token.txt"):
        if args.email:
            cards = login(input("Mobalytics Email: "), getpass.getpass("Mobalytics Password: "))
        else:
            print("Need your email and password, type --help to get help.")
            sys.exit(0)
    else:
        cards = login("", "", True)
    if args.deck:
        ask_cards(args.deck, cards)
    else:
        print("Need your deck code, type --help to get help.")
        sys.exit(0)

    

if __name__ == "__main__":
    main()