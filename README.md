# Legends of Runeterra Deck Cost Calculator

This is a simple deck cost calculator from a deck code that is given to the program.

![example](https://gitlab.com/burningsunrise/lor-deck-cost/-/raw/master/example.png)

## Basic Usage

To use this script you need to have a mobalytics account with your cards on correctly imported, you can create one at their website and get their app to load in your cards [mobalytics](https://lor.mobalytics.gg). After you have done this, you can use this script to calculate deck cost based on what you already have, you will need to provide it with the number of wildcards in your possession as mobalytics does not accurately keep track of this.

Make sure you install the requirements
```
pip3 install -r requirements.txt
```

Example Usage Signing in and giving it a deck, afterwards you can just use the `-d` flag with your deck code, as we save your token to a token.txt file, if you are having issues with using the token, simply delete this file and have it refresh. 
```python
python3 deck_cost.py -e -d CECAIAIABEKR2MYDAIDBMOR6AMBAAAQGBEAQGAAKAMAQEAAHAEBQADQBAIDAQAIBAEACK
```

If there are any questions, bugs, or feature requests please open an issue on gitlab/github! :dog: